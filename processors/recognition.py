import face_recognition
import cv2
import numpy as np
import os
import glob
#from playsound import playsound
#from PIL import Image, ImageDraw
import logging
from datetime import date, datetime, timedelta
import time
import re
from utils.log_thread import LogThread
import threading

class Recognition:
    def __init__(self, log_level, tolerance):
        logging.basicConfig(level=log_level)
        self.log_thread = LogThread()

        # Set tolerance for face recognition, lower is more strict default 0.6
        self.tolerance = tolerance

        # TODO: Add a way to add new people to the recognition system
        # TODO: Add a way to remove people from the system
        # TODO: Add a way to add new people to the access list (database/ui)
        # TODO: Add a way to remove people from the access list (database/ui)
        # initialize the known faces and the face detector
   

        self.access_list = ['Matt Hardy', 'Taylor Swift']

        #TODO: Store known photo data in a database (may be load heavy, process in threads?)
        # store the photos and names of the people in the database
        self.faces_encodings = []
        self.faces_names = []
        self.face_locations = []
        self.process_this_frame = True

        # TODO: better way to do this? Possibly a single object with in, out, denied
        # Track access to the door for timers to limit access time
        self.recent_access = {}
        self.recent_exit = {}
        self.recent_denied = {}
        self.time_accessed = datetime.now()
        # TODO: AWS S3 bucket for images learn on upload and add to database
        # Path to the folder with the images and list of files
        self.cur_direc = os.getcwd()
        self.path = os.path.join(self.cur_direc, 'images/')
        # self.path = os.path.join(self.cur_direc, '../tmp_img/img_align_celeba/')
        self.unknown_path = os.path.join(self.cur_direc, 'unknown_images/')
        self.list_of_files = [f for f in glob.glob(self.path+'*')]
        self.number_files = len(self.list_of_files)
        self.names = self.list_of_files.copy()

        # TODO: better sounds for each action
        # Sounds
        self.sound_path = os.path.join(self.cur_direc, 'sounds/')

        # Video capture
        self.video_capture = cv2.VideoCapture(0)

        #sleep time
        self.sleep_time = 0.3
        self.process_this_frame = True

    def sleep(self):
        time.sleep(float(self.sleep_time))

    def log_access(self, name, action, time = datetime.now().strftime("%m/%d/%Y, %-I:%M:%S%p")):
        self.log_thread.access_log(name, action, time)

        # If log_level debug/info log access to stdout
        logging.info("{} {} {}".format(name, action, time))

    def log_name(self, name, action = 'learned', time = datetime.now().strftime("%m/%d/%Y, %-I:%M:%S%p")):
        self.log_thread.known_names_log(name, 'learned', time)

        # If log_level debug, info log access to stdout
        logging.info("{} {} {}".format(name, action, time))
        
        

    def build_name(self, name: str):
        # Remove the file extension and path, make proper name
        name = name.replace(self.cur_direc + "/images/", "") 
        name = re.sub(r'\..*',"",name) # Remove extension
        name = name.replace("-", " ") # make name presentable
        return name.title()

    def save_image_thread(self, frame, frame_num):
        cv2.imwrite(self.unknown_path + 'unknown-' + str(frame_num) + '.jpg', frame)
        self.log_access('unknown-' + str(frame_num), 'denied, image_saved')
        # TDOD: search for matches to identity the unknown person online and train them

    def learn_faces(self):
        for i in range(self.number_files):
            try:
                globals()['image_{}'.format(i)] = face_recognition.load_image_file(self.list_of_files[i])
                globals()['image_encoding_{}'.format(i)] = face_recognition.face_encodings(globals()['image_{}'.format(i)])[0]
                self.faces_encodings.append(globals()['image_encoding_{}'.format(i)])

                # If debug, log image info to stdout              
                logging.debug(globals()['image_{}'.format(i)])
                logging.debug(globals()['image_encoding_{}'.format(i)].shape)

                # Create array of known names
                name = self.build_name(self.names[i])
                self.log_name(name)
                self.faces_names.append(name)
            except IndexError:
                print("No faces found in the image {}".format(self.list_of_files[i]))
                continue

    def draw_detection_box(self, frame, left, top, right, bottom, box_color, text_color, access_text, name):
        font = cv2.FONT_HERSHEY_TRIPLEX
        # Draw a rectangle around the face
        # Input text label with a name below the face, access granted or denied
        cv2.rectangle(frame, (left-5, top-5), (right+5, bottom+5), box_color, 1)
        cv2.rectangle(frame, (left-5, top - 40), (right+5, top+5), box_color, cv2.FILLED)
        cv2.putText(frame, access_text, (left + 6, top - 6), font, .80, text_color, 1)
        cv2.rectangle(frame, (left-5, bottom - 40), (right+5, bottom+5), box_color, cv2.FILLED)
        cv2.putText(frame, name, (left + 6, bottom - 6), font, .80, text_color, 1)

    def display_access_results(self, frame, frame_num):
        
        # Grant access or deny access
        for (top, right, bottom, left), name in zip(self.face_locations, self.face_names):
            top *= 4
            right *= 4
            bottom *= 4
            left *= 4
            access_text = '(' + str(abs(round((datetime.now()-self.time_accessed).total_seconds()))) + ') Analyzing...'
            box_color = (0, 0, 0, 255)
            text_color = (0, 0, 255)

            if name == 'Unknown' or name not in self.access_list:
                access_text = 'Access Denied'
                box_color = (0, 0, 255, 50)
                text_color = (255, 255, 255)
                if name not in self.recent_denied.keys() or name == 'Unknown' or self.recent_denied[name]['time_denied'] + timedelta(seconds = 10) <= datetime.now():
                    self.log_access(name, 'denied')
                    # TODO: lock out
                    try:
                        if frame_num % 10 == 0:
                            # Save an image of the unknown face every tenth frame, threaded
                            save_image = threading.Thread(target = self.save_image_thread, args=(frame, frame_num), daemon=True)  
                            save_image.start()

                        del self.recent_denied[name]                   
                        self.recent_denied[name] = {'time_denied': datetime.now()}
                        self.time_accessed = datetime.now() + timedelta(seconds = 10)
                    except:
                        pass
            elif name in self.recent_access:
                if name not in self.recent_exit.keys() and self.recent_access[name]['time_in'] + timedelta(seconds = 5) <= datetime.now():
                    access_text = 'Exit Granted'    
                    box_color = (110, 200, 255, 255)
                    text_color = (0, 0, 0)            
                    try:
                        self.log_access(name, 'exit')
                        # playsound.playsound(self.sound_path + 'ding.mp3')
                        # TODO: unlock for x seconds
                        self.recent_exit[name] = {'time_out': datetime.now()}
                        self.time_accessed = datetime.now() + timedelta(seconds = 5)
                        del self.recent_access[name]
                    except:
                        pass 
            else:
                if name not in self.recent_exit.keys() or self.recent_exit[name]['time_out'] + timedelta(seconds = 5) <= datetime.now():
                    access_text = 'Access Granted'
                    box_color = (0, 255, 0, 50)
                    text_color = (0, 0, 0)
                    try:
                        self.log_access(name, 'access')
                        # playsound.playsound(self.sound_path + 'ding.mp3')
                        # TODO: unlock for x seconds
                        self.recent_access[name] = {'time_in': datetime.now()}
                        self.time_accessed = datetime.now() + timedelta(seconds = 5)
                        del self.recent_exit[name]
                    except:
                        pass 

            self.draw_detection_box(
                frame,
                left,
                top,
                right,
                bottom,
                box_color,
                text_color,
                access_text,
                name
            )
            
        # Display the resulting image
        cv2.imshow('Video', frame)
        self.sleep()

    def detect_faces(self, frame, frame_num):
        # Process every 10th frame
        try:
            if frame_num % 10 == 0:
                self.process_this_frame = True
            try:
                small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
            except:
                small_frame = frame
            rgb_small_frame = small_frame[:, :, ::-1]
            if self.process_this_frame:
                self.face_locations = face_recognition.face_locations( rgb_small_frame)
                self.face_encodings = face_recognition.face_encodings( rgb_small_frame, self.face_locations)
                self.face_names = []
                for face_encoding in self.face_encodings:
                    matches = face_recognition.compare_faces (self.faces_encodings, face_encoding, self.tolerance)
                    name = "Unknown"
                    face_distances = face_recognition.face_distance( self.faces_encodings, face_encoding)

                    # Log distance to frame from known faces
                    logging.debug(face_distances)
                    best_match_index = np.argmin(face_distances)
                    if matches[best_match_index]:
                        name = self.faces_names[best_match_index]
                    self.face_names.append(name)
        except:
            pass
        self.process_this_frame = not self.process_this_frame    
