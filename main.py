import cv2
from processors.recognition import Recognition
import logging
import click

@click.command()
@click.option('--tolerance', prompt='Enter tolerance', default=.6, help='Enter matching tolerance')

def run(tolerance):
    frame_num = 0
    # Initialize the recognition class 
    # log levels: DEBUG, INFO, WARNING, ERROR, CRITICAL

    rec = Recognition(logging.INFO, tolerance)

    # learn the face encodings from the images in the training set (./images)
    rec.learn_faces()

    # Begin video stream and recognition/access processing
    while True:
        ret, frame = rec.video_capture.read()
        rec.detect_faces(frame, frame_num)
        rec.display_access_results(frame, frame_num)
        frame_num += 1

        # 'q' to quit
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

def main():
    run()

if __name__ == '__main__':
    main()