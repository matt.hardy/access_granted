from .logger import Logger
import threading
from datetime import datetime
import time

class LogThread():  
    def __init__(self):
        self.access_logger = Logger('logs/access_log-' + datetime.now().strftime("%m-%d-%Y") + '.txt')
        self.known_names_logger = Logger('logs/known_names_log-' + datetime.now().strftime("%m-%d-%Y") + '.txt', 'w')

    def access_log_thread(self, name, action, time):
        self.access_logger.log("Name: {}, Action: {}, Time: {}".format(name, action, time))

    def known_names_thread(self, name, action, time):
        self.known_names_logger.log("Name: {}, Action: {}, Time: {}".format(name, action, time))

    def access_log(self, name, action, time):
        access_thread = threading.Thread(target = self.access_log_thread, args=(name, action, time))   
        access_thread.start()

    def known_names_log(self, name, action, time):
        name_thread = threading.Thread(target = self.known_names_thread, args=(name, action, time))   
        name_thread.start()

    def run(self):
        while True:
            time.sleep(1)        