class Logger:
    def __init__(self, log_file_name, access = 'a+'):
        self.log_file_name = log_file_name
        self.log_file = open(log_file_name, access)

    def log(self, message):
        self.log_file.write(message + '\n')
        self.log_file.flush()

    def close(self):
        self.log_file.close()