ACCESS GRANTED FACIAL RECOGNITION

Usage: 
____________________________________________________________
    - create a logs folder in the project root

    - run `python main.py` from root in a terminal

    - you will be prompted for tolerance (default: 0.6) lower is more strict match

    - adjust log level when initializing Recognition in main()

Add images to the image folder.
_____________________________________________________________

    - naming will be firstName-lastName.extension 

    - ie. `steve-jobs.jpg`

    - jpg, jpeg, png are supported

You can test the application by finding an image of one of the people in the images folder and opening it on your phone for the computers camera to scan.
Future functionality will be incorporating gesture recognition to prevent people from being able to log in as someone else.

If a user is unknown it will save a captured image in the unknown_images folder for record
logs will be saved by date with timestamps of all access attempts in the logs folder you created
This is just a POC so the code has not been refactored and cleaned up as it would be in a production environment.
Have fun
Matt Hardy